# Git Command Reference

## Cloning Repository

```bash
sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git
$ git clone -b master https://gitlab.com/sanjay0206/jsp-registration.git
Cloning into 'jsp-registration'...
remote: Enumerating objects: 64, done.
remote: Counting objects: 100% (64/64), done.
remote: Compressing objects: 100% (43/43), done.
remote: Total 64 (delta 17), reused 31 (delta 7), pack-reused 0 (from 0)
Receiving objects: 100% (64/64), 6.99 KiB | 1022.00 KiB/s, done.
Resolving deltas: 100% (17/17), done.


sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git
$ ls -lrt
total 4
drwxr-xr-x 1 sanja 197609 0 May 11 11:49 jsp-registration/


sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ ls -lrt
total 4
drwxr-xr-x 1 sanja 197609    0 May 11 11:49 src/
-rw-r--r-- 1 sanja 197609 1772 May 11 11:49 pom.xml


sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git status
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean

sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   src/main/java/com/app/entity/Person.java

no changes added to commit (use "git add" and/or "git commit -a")
```

## Staging Person.java file
```bash
sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git add .

sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   src/main/java/com/app/entity/Person.java


sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git commit -m "toString method added to Person entity"
[master 2d21062] toString method added to Person entity
 1 file changed, 9 insertions(+), 1 deletion(-)

```

## Push the change
```bash
sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git push
Enumerating objects: 17, done.
Counting objects: 100% (17/17), done.
Delta compression using up to 8 threads
Compressing objects: 100% (5/5), done.
Writing objects: 100% (9/9), 630 bytes | 315.00 KiB/s, done.
Total 9 (delta 2), reused 0 (delta 0), pack-reused 0
To https://gitlab.com/sanjay0206/jsp-registration.git
   2ed9888..352c087  master -> master

```

## Unstaging web.xml file
```bash
sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   src/main/webapp/WEB-INF/web.xml

no changes added to commit (use "git add" and/or "git commit -a")

sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git add .

sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   src/main/webapp/WEB-INF/web.xml
        
```
#### Unstaging using - git reset
```bash
sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git reset src/main/webapp/WEB-INF/web.xml
Unstaged changes after reset:
M       src/main/webapp/WEB-INF/web.xml

sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   src/main/webapp/WEB-INF/web.xml

no changes added to commit (use "git add" and/or "git commit -a")

```

## Git fetch
```bash
sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git fetch
remote: Enumerating objects: 11, done.
remote: Counting objects: 100% (11/11), done.
remote: Compressing objects: 100% (5/5), done.
remote: Total 6 (delta 2), reused 0 (delta 0), pack-reused 0 (from 0)
Unpacking objects: 100% (6/6), 510 bytes | 24.00 KiB/s, done.
From https://gitlab.com/sanjay0206/jsp-registration
   2d21062..2ed9888  master     -> origin/master


sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git status
On branch master
Your branch is behind 'origin/master' by 1 commit, and can be fast-forwarded.
  (use "git pull" to update your local branch)

nothing to commit, working tree clean
```
### To check last 2 commits in master branch
```bash
sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git log -n 2 master
commit 2d2106259bf420f710b124d19e8c6b1cec1658fc (HEAD -> master)
Author: sanjay0206 <62499148+sanjay0206@users.noreply.github.com>
Date:   Sat May 11 11:57:03 2024 +0530

    toString method added to Person entity

commit c7c806a2dd425a28e74ad5aa692b9dc1863ff4f2
Author: Sanjay P <sanjup0206@gmail.com>
Date:   Thu May 9 17:53:53 2024 +0000

    Update success.jsp
```
## Git pull
```bash
sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git pull origin master
From https://gitlab.com/sanjay0206/jsp-registration
 * branch            master     -> FETCH_HEAD
Updating 2d21062..2ed9888
Fast-forward
 src/main/webapp/index.jsp | 1 +
 1 file changed, 1 insertion(+)
 
```
## Creating new branch feature and deleting it
```bash
sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git branch feature

sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git checkout feature
Switched to branch 'feature'

sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (feature)
$ git branch
* feature
  master
```

### Deleting a branch:
```bash 
sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (feature)
$ git branch -D feature
error: Cannot delete branch 'feature' checked out at 'C:/Users/sanja/OneDrive/Desktop/git/jsp-registration'


sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (feature)
$ git checkout master
Switched to branch 'master'
Your branch is behind 'origin/master' by 1 commit, and can be fast-forwarded.
  (use "git pull" to update your local branch)


sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git branch
  feature
* master

sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git branch -D feature
Deleted branch feature (was 2d21062).

```
## Git diff
```bash
sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (master)
$ git checkout feature
Switched to branch 'feature'

sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (feature)
$ git status
On branch feature
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   src/main/webapp/error.jsp

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   src/main/webapp/WEB-INF/web.xml
        modified:   src/main/webapp/error.jsp

sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (feature)
$ git diff
diff --git a/src/main/webapp/WEB-INF/web.xml b/src/main/webapp/WEB-INF/web.xml
index d80081d..eea6b10 100644
--- a/src/main/webapp/WEB-INF/web.xml
+++ b/src/main/webapp/WEB-INF/web.xml
@@ -3,4 +3,10 @@
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
          version="4.0">
+
+    <error-page>
+        <error-code>404</error-code>
+        <location>/error.jsp</location>
+    </error-page>
+
:

sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (feature)
$ git diff master
diff --git a/src/main/webapp/WEB-INF/web.xml b/src/main/webapp/WEB-INF/web.xml
index d80081d..eea6b10 100644
--- a/src/main/webapp/WEB-INF/web.xml
+++ b/src/main/webapp/WEB-INF/web.xml
@@ -3,4 +3,10 @@
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
          version="4.0">
+
+    <error-page>
+        <error-code>404</error-code>
+        <location>/error.jsp</location>
+    </error-page>
+
:


sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (feature)
$ git status
On branch feature
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   src/main/webapp/error.jsp

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   src/main/webapp/WEB-INF/web.xml
        modified:   src/main/webapp/error.jsp


sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (feature)
$ git add .

sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (feature)
$ git status
On branch feature
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   src/main/webapp/WEB-INF/web.xml
        new file:   src/main/webapp/error.jsp


sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (feature)
$ git diff
```
## Git commit
```bash
sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (feature)
$ git commit -m "404 Error page is added"
[feature 7bd8c03] 404 Error page is added
 2 files changed, 17 insertions(+)
 create mode 100644 src/main/webapp/error.jsp

sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (feature)
$ git push 
fatal: The current branch feature has no upstream branch.
To push the current branch and set the remote as upstream, use

    git push --set-upstream origin feature


sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (feature)
$  git push --set-upstream origin feature
Enumerating objects: 14, done.
Counting objects: 100% (14/14), done.
Delta compression using up to 8 threads
Compressing objects: 100% (6/6), done.
Writing objects: 100% (8/8), 917 bytes | 917.00 KiB/s, done.
Total 8 (delta 2), reused 0 (delta 0), pack-reused 0
remote: 
remote: To create a merge request for feature, visit:
remote:   https://gitlab.com/sanjay0206/jsp-registration/-/merge_requests/new?merge_request%5Bsource_branch%5D=feature
remote:
To https://gitlab.com/sanjay0206/jsp-registration.git
   2ed9888..7bd8c03  feature -> feature
Branch 'feature' set up to track remote branch 'feature' from 'origin'.

```

## Changing commit message using git rebase
```bash

sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (feature)
$ git log --oneline -5
7bd8c03 (HEAD -> feature, origin/feature) 404 Error page is added
2ed9888 (origin/master, origin/HEAD, master) Update index.jsp
2d21062 toString method added to Person entity
c7c806a Update success.jsp
d7f1105 new commit

sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (feature)
$ git rebase -i HEAD~3
[detached HEAD 61d8dd4] 404 Error page is added | reworded
 Date: Sat May 11 13:15:46 2024 +0530
 2 files changed, 17 insertions(+)
 create mode 100644 src/main/webapp/error.jsp
Successfully rebased and updated refs/heads/feature.

Editor 1: change pick to reword or r 
pick 2d21062 toString method added to Person entity
pick bcae654 Update index.jsp
pick 2c48c94 404 Error page is added

pick 2d21062 toString method added to Person entity
pick bcae654 Update index.jsp
reword 2c48c94 404 Error page is added

Editor 2:  404 Error page is added -> will appear so change the commit message
404 Error page is added | reworded

sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (feature)
$ git push origin feature --force
Enumerating objects: 14, done.
Counting objects: 100% (14/14), done.
Delta compression using up to 8 threads
Compressing objects: 100% (6/6), done.
Writing objects: 100% (8/8), 931 bytes | 465.00 KiB/s, done.
Total 8 (delta 2), reused 0 (delta 0), pack-reused 0
remote:
remote: To create a merge request for feature, visit:
remote:   https://gitlab.com/sanjay0206/jsp-registration/-/merge_requests/new?merge_request%5Bsource_branch%5D=feature
remote:
To https://gitlab.com/sanjay0206/jsp-registration.git
 + 2c48c94...61d8dd4 feature -> feature (forced update)
```

### Abort a rebase:
```bash
sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (feature|REBASE 1/5)
$ git rebase --abort

sanja@DESKTOP-2I4L826 MINGW64 ~/OneDrive/Desktop/git/jsp-registration (feature)
$
```